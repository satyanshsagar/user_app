const express = require('express');
const bodyparser = require('body-parser');
const session = require('express-session');
const commonMiddlewares = require('./routes/common_middlewares/common').router;
const logger = require('./mylogger');
const route = require('./routes/router');
const bookroute = require('./routes/books/bookroute');
const authorroute = require('./routes/authors/authorroute');
const userroute = require('./routes/user_routes/userroute');

const server = express();
server.use(session({ secret: 'notsecret', resave: false, saveUninitialized: true }));
server.use(bodyparser.urlencoded({ extended: false }));
server.use(bodyparser.json());
server.set('view engine', 'ejs');
server.use(express.static('public'));
server.use(commonMiddlewares);
server.use(route);
server.use(userroute);
server.use(bookroute);
server.use(authorroute);

server.use((error, req, res, next) => {
  res.writeHead(404, { 'content-type': 'text/plain' });
  res.end(`Error 404: ${error.message}`);
  next();
});

const myServer = server.listen(process.env.PORT || 4000, () => {
  logger(`server is live at address: http://localhost and port: ${myServer.address().port}`);
});

module.exports = server;
