const logger = require('../../mylogger');
const request = require('request-promise');
const {authentication_api} = require('../../config/config');

async function authenticate(userdata) {
  let finalresult;
  try {
    const option = {
    headers: { "content-type": "application/json" },
    method : 'POST',
    uri: `${authentication_api}/loginuser`,
    body: JSON.stringify(userdata)
      };
       finalresult =await request(option);
  }
  catch(e) {
    logger(e.stack);
  }
  return finalresult;
}



async function register(userdata) {
  let finalresult;
  try {
    const option = {
      method:'POST',
      uri: `${authentication_api}/registeruser`,
      headers:{'content-type':'application/json'},
      body: JSON.stringify(userdata)
    };
    finalresult = await request(option);
  } catch (e) {
    logger(e.stack);
  }
return finalresult;
}


module.exports = {
  authenticate,
  register
};
