const router = require('express').Router();
const usercontroller = require('./usercontroller');

router.post('/loginuser', async (req, res) => {
  const userdata = { email: req.body.email, password: req.body.password };
  let result = await usercontroller.authenticate(userdata);
  result = JSON.parse(result);
  console.log(result);
  if (result.user) {
    req.session.username = userdata.email;
    res.redirect('/home');
  } else {
    res.render('error', { message: 'wrong password/user email..!' });
  }
});

router.post('/registeruser', async (req, res) => {
  const userdata = { email: req.body.email, password: req.body.password, username: req.body.username };
  console.log(userdata);
  const result = await usercontroller.register(userdata);
  if (result) {
    res.redirect('/');
  } else {
    res.render('error', { message: 'error registering user' });
  }
});

module.exports = router;
