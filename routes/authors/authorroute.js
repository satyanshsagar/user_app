const router = require('express').Router();
const authorController = require('./authorcontroller.js');
const nocache = require('../common_middlewares/common').nocaching;
const checkSession = require('../common_middlewares/common').checkSession;


router.get('/authors', nocache, checkSession, async (req, res) => {
  // get all authors
  const authordata = await authorController.getallauthors();
  res.render('authors', { authors: authordata });
});

router.get('/authors/:id', nocache, checkSession, async (req, res) => {
  const id = parseInt(req.params.id, 10);
  const author = await authorController.getauthor(id);
  console.log(author);
  if (author.length > 0) {
    res.render('authordescription', author[0]);
  } else {
    res.writeHead(404, { 'content-type': 'text/plain' });
    res.end('Error 404: Content Not found!!!');
  }
});
module.exports = router;
