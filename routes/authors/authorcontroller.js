const logger = require('../../mylogger');
const request = require('request-promise');
const {library_api} = require('../../config/config');

async function getauthor(authorID) {
  let result;
  try {
    const option = {
      uri:`${library_api}/authors/${authorID}`,
      json:true
    };
    result = await request(option);
  } catch (e) {
    logger(e.stack);
  }
  return result;
}


async function getallauthors() {
  let result;
  try {
    const option = {
      uri:`${library_api}/authors`,
      json:true
    };
    result = await request(option);
  } catch (e) {
    logger(e.stack);
  }
  return result;
}

module.exports = {
  getauthor,
  getallauthors
};
