const router = require('express').Router();
const nocache = require('./common_middlewares/common').nocaching;
const checkSession = require('./common_middlewares/common').checkSession;

router.get('/', nocache, (req, res) => {
  res.render('login');
});

router.get('/register', (req, res) => {
  res.render('register');
});

router.get('/home', nocache, checkSession, (req, res) => {
  res.render('home');
});

router.get('/logout', (req, res) => {
  req.session.destroy();
  res.redirect('/');
});

module.exports = router;
