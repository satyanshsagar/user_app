const router = require('express').Router();
const logger = require('../../mylogger');
const bookController = require('./bookcontroller.js');
const nocache = require('../common_middlewares/common').nocaching;
const checkSession = require('../common_middlewares/common').checkSession;

router.get('/books', nocache, checkSession, async (req, res) => {
  // get all books
  const bookdata = await bookController.getallbooks();
  res.render('books', { books: bookdata });
});

router.get('/books/:index', nocache, checkSession, async (req, res) => {
  // const isbn = parseInt(req.params.index, 10);
  const isbn = req.params.index;
  // get book by isbn
  try {
    const book = await bookController.getbook(isbn);
    const review = await bookController.getReviewsByIsbn(isbn);
    if (book.length > 0) {
      res.render('bookdescription', { book: book[0], review });
    } else {
      res.writeHead(404, { 'content-type': 'text/plain' });
      res.end('Error 404: Content Not found!!!');
    }
  } catch (e) {
    logger(e.stack);
  }
});

module.exports = router;
