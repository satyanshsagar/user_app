const fetch = require('node-fetch');
const parser = require('xml2json');
const bluebird = require('bluebird');
const logger = require('../../mylogger');
const request =require('request-promise');
const {library_api} = require('../../config/config');

fetch.Promise = bluebird;

async function getReviewsByIsbn(isbn) {
  let finalresult;
  try {
    const url = `https://www.goodreads.com/book/isbn/${isbn}?key=TrlEZAqoZ5oCeG5BLe7g`;
    const result = await fetch(url);
    const body = await result.text();
    const widget = JSON.parse(parser.toJson(body));
    finalresult = widget.GoodreadsResponse.book.reviews_widget;
  } catch (e) {
    logger(e.message);
  }
  return finalresult;
}

async function getbook(isbn) {
  let result;
  try {
    const option = {
      uri:`${library_api}/books/${isbn}`,
      json:true
    };
    result = await request(option);
  } catch (e) {
    logger(e.stack);
  }
  return result;
}

async function getallbooks() {
  let result;
  try {
    const option = {
      uri:`${library_api}/books`,
      json:true
    };
    result = await request(option);
  } catch (e) {
    logger(e.stack);
  }
  return result;
}

module.exports = {
  getallbooks,
  getbook,
  getReviewsByIsbn
};
