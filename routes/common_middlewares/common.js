const router = require('express').Router();
const logger = require('../../mylogger');


// handling cache problem
function nocaching(req, res, next) {
  res.set('Cache-Control', 'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0');
  next();
}

// logging requests made to the server
router.use((req, res, next) => {
  const d = new Date();
  const date = `${d.getHours()}:${d.getMinutes()}:${d.getSeconds()}   ${d.getDate()}/${d.getMonth()}/20${d.getYear() - 100}`;
  logger(`URL: ${req.url} METHOD: ${req.method} TIMESTAMP: ${date}`);
  next();
});

function checkSession(req, res, next) {
  if (!req.session.username) {
    res.render('login');
  } else {
    next();
  }
}

module.exports = {
  router,
  nocaching,
  checkSession
};
