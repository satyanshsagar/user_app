const winston = require('winston');

const logger = winston.createLogger({
  level: 'info',
  format: winston.format.json(),
  transports: [
    new winston.transports.Console()
  ]
});

const winlog = (logData) => {
  logger.log({
    level: 'info',
    message: logData
  });
};
module.exports = winlog;
